import os
# import time
# from datetime import datetime
from json import dumps, loads
import sys
from os import environ
from subprocess import run

command = ["arp", "-a"]
print(command)
result = run(command, capture_output=True, shell=True)
data = result.stdout.decode("utf-8")
print(data)
IPs = []
for i in data.split('\n'):
    if "10.0.0." in i:
        ip = i.split('(')[1].split(')')[0]
        IPs.append(ip)
print(IPs)

lookup = {
    '10.0.0.227': 'macbook',
    '10.0.0.86':  'playstation',
    '10.0.0.0':   'desktop',
    '10.0.0.133': 'galaxy-s10',
    '10.0.0.22': 'pixel',
    '10.0.0.55': 'SFCOF',
    '10.0.0.111': 'netgear',
    '10.0.0.60':  'hue1',
    '10.0.0.112': 'hue2',
    '10.0.0.163': 'hue3',
    '10.0.0.232': 'hue4',
    '10.0.0.108': 'hue5',
    '10.0.0.168': 'hue6',
    '10.0.0.20':  'hue7',
    '10.0.0.100': 'hue8',
    '10.0.0.183': 'remote1',
    '10.0.0.110': 'socket1',
    '10.0.0.110': 'socket2',
    '10.0.0.110': 'socket5',
    '10.0.0.110': 'socket4',
    '10.0.0.110': 'socket5',
    '10.0.0.240': 'jarvis',
    '10.0.0.2':   'grove',
    '10.0.0.0':   'octopi',
    '10.0.0.0':   'bedroom',
    '10.0.0.0':   'office',
    '10.0.0.0':   'kitchen',
    '10.0.0.0':   'lounge',
    '10.0.0.0':   'diningroom',
    '10.0.0.0':   'hallway',
}
ignore = ["10.0.0.254", "10.0.0.1"]
devices = [{'name': lookup.get(ip, "?"), 'IP': ip} for ip in IPs if ip not in ignore]

devices = sorted(devices, key=lambda x: x['name'])#, reverse=True)

print(devices)
message = dumps(devices)
print(message)

import paho.mqtt.client as mqtt
name = 'ARP'
client = mqtt.Client(client_id=name)
mqtt_host = environ.get('broker', "10.0.0.240")
client.connect(mqtt_host)
print("Connected")
topic = "ARP/data"
client.publish(topic, message)
